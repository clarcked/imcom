// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path")
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CopyWebpackPlugin = require('copy-webpack-plugin');

// const renderAssets = ['img', 'css', 'fonts'].map(asset => {
//     return new CopyWebpackPlugin([
//         {
//             patterns: [
//                 {
//                     from: path.resolve(__dirname, 'src', asset),
//                     to: path.resolve(__dirname, '.webpack/renderer', asset)
//                 }
//             ]
//         }
//     ]);
// })
const mainAssets = new CopyWebpackPlugin(
    {
        patterns: [
            {
                from: path.resolve(__dirname, 'src', "assets"),
                to: path.resolve(__dirname, '.webpack/main', "assets")
            }
        ]
    }
)

module.exports = [
    new ForkTsCheckerWebpackPlugin(),
    mainAssets
];
