import { FaTruckLoading as StockIcon } from "react-icons/fa";
import { FiUsers as CustomerIcon } from "react-icons/fi";
import { RiShoppingCart2Fill as CartIcon, RiWifiOffLine as OfflineIcon, RiWifiLine as OnlineIcon } from "react-icons/ri";
import {
  RiSearchLine as SearchIcon,
  RiTruckFill as ProviderIcon,
} from "react-icons/ri";
import { HiOutlineTag as ProductIcon } from "react-icons/hi";
import { BiSupport as SupportIcon } from "react-icons/bi";
import { MdClose as CloseIcon } from "react-icons/md";

export {
  SearchIcon,
  StockIcon,
  CartIcon,
  CustomerIcon,
  ProductIcon,
  ProviderIcon,
  SupportIcon,
  CloseIcon,
  OnlineIcon,
  OfflineIcon
};
