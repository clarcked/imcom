import React from "react";
import cclass from "../../helpers/cclass";

const Button = (props: any) => {
  return (
    <button
      className={cclass(["btn", props?.className])}
      type={props.type ?? "button"}
    >
      {props?.label}
    </button>
  );
};

export default Button;
