import * as React from "react";
import * as ReactDOM from "react-dom";
import { Route, Switch, HashRouter as Browser, Link } from "react-router-dom";
import ImCom from "../../core/graphx/layouts/imcom";
import { CreateProvider, ListProvider } from "../provider";
import { CreateOrder, ListOrder } from "../order";
import MainMenu from "./menu";
import StatusBar from "../../core/graphx/widgets/stats/statusbar";
import { OfflineIcon } from "../../core/graphx/icons";

const App = (): JSX.Element => {
  return (
    <Browser>
      <ImCom>
        <main>
          <section>
            <MainMenu />
            <div className="screen">
              <Switch>
                <Route path={["/", "order/list"]} component={ListOrder} exact />
                <Route path={["/provider/create"]} component={CreateProvider} />
                <Route path={["/provider/list"]} component={ListProvider} />
              </Switch>
            </div>
          </section>
          <aside>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
            modi at incidunt accusantium! Est reprehenderit perferendis animi
            tenetur. Unde, tempora?
          </aside>
        </main>
        <footer> 
          <StatusBar label={"Offline"} Icon={<OfflineIcon />} />
        </footer>
      </ImCom>
    </Browser>
  );
};

(function render() {
  ReactDOM.render(<App />, document.getElementById("root"));
})();
