const cclass = (list: any[]): string => {
  if (!Array.isArray(list)) {
    return "";
  }
  return list.join(" ");
};

export default cclass