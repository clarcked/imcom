import React from "react";
import cclass from "../../../helpers/cclass";
import style from "./style.scss";

const StatusBar = ({ Icon, label, className, onClick }: any) => {
  return (
    <button
      type="button"
      onClick={onClick}
      className={cclass([style.container, className])}
    >
      <div className={cclass([style.icon])}>{Icon}</div>
      <div className={cclass([style.label])}>{label}</div>
    </button>
  );
};

export default StatusBar;
