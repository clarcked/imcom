import child from "child_process"
import winston from "winston"

const {combine, timestamp, label, printf} = winston.format;

const logFormat = printf(({level, message, label, timestamp}) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const options = {
    file: {
        level: 'debug',
        filename: './logs/app.log',
        handleExceptions: true,
        format: combine(
            label({label: `pid:${process.pid}`}),
            timestamp(),
            logFormat
        ),
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'info',
        handleExceptions: true,
        colorize: true,
        format: combine(
            label({label: `pid:${process.pid}`}),
            timestamp(),
            logFormat
        ),
    },
};

const logger = winston.createLogger({
    levels: winston.config.npm.levels,
    transports: [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console)
    ],
    exitOnError: false
})

export default logger