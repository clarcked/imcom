import React from "react";
import { Link } from "react-router-dom";
import {
  StockIcon,
  ProviderIcon,
  ProductIcon,
  CustomerIcon,
  SupportIcon,
  CartIcon,
  SearchIcon,
} from "../../core/graphx/icons";

const MainMenu = (props: any) => {
  return (
    <nav className="nav nav-v">
      <ul>
        <li>
          <Link to="search" className="hasicon">
            <SearchIcon size={20} />
          </Link>
        </li>
        <li>
          <Link to="/" className="hasicon active">
            <CartIcon size={20} />
          </Link>
        </li>
        <li>
          <Link to="stock/create" className="hasicon">
            <StockIcon size={20} />
          </Link>
        </li>
        <li>
          <Link to="provider/list" className="hasicon">
            <ProviderIcon size={20} />
          </Link>
        </li>
        <li>
          <a href="#" className="hasicon">
            <ProductIcon size={20} />
          </a>
        </li>
      </ul>
      <ul className="bottom">
        <li>
          <a href="#" className="hasicon">
            <CustomerIcon size={20} />
          </a>
        </li>
        <li>
          <a href="#" className="hasicon">
            <SupportIcon size={20} />
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default MainMenu;
