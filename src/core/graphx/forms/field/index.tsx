import React from "react";
import cclass from "../../helpers/cclass";

const InputField = (props: any) => {
  return (
    <>
      {props.label && <div className="label">{props.label}</div>}
      <label className={cclass([props.className, "control"])}>
        {props.IconLeft && <div className="icon">{props.IconLeft}</div>}
        <input
          type={props.type}
          name={props.name}
          placeholder={props.placeholder}
        />
        {props.IconRight && <div className="icon">{props.IconRight}</div>}
      </label>
    </>
  );
};
export default InputField;
