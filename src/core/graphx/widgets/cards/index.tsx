import React from "react";
import cclass from "../../helpers/cclass";
import { CloseIcon, ProviderIcon } from "../../icons";

const Card = ({ Icon, Image, className, children }: any) => {
  return (
    <div className={cclass(["card", className])}>
      <div className="card-body">
        <button className="card-thumbnail" type="button">
          {Icon && <div className="icon">{Icon}</div>}
          {Image && <div className="thumb">{Image}</div>}
        </button>
        <button className="card-content" type="button">
          {children}
        </button>
      </div>
    </div>
  );
};

export default Card;
