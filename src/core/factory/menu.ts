import {app, Menu, MenuItem} from "electron";
import EventManager from "../manager/event";

class MenuFactory {
    private readonly menu: Menu
    private readonly items: { [key: string]: MenuItem } = {}
    private eventMan: EventManager

    constructor() {
        this.menu = new Menu()
        this.setItem({
            "File": {
                submenu: [
                    {
                        label: "New Cart",
                        accelerator: process.platform === 'darwin' ? 'Command+N' : 'Ctrl+N',
                        click: () => {
                            this.eventMan?.handleEvent("new cart")
                        }
                    }, {
                        label: "Preferences",
                        click: () => {
                            this.eventMan?.handleEvent("preferences")
                        }
                    }, {
                        label: "Exit",
                        accelerator: process.platform === 'darwin' ? 'Command+Q' : 'Alt+F4',
                        click: () => {
                            app.quit()
                        }
                    }
                ]
            },
            "Register": {
                click: () => {
                    this.eventMan?.handleEvent("register")
                }
            },
            "Help": {
                submenu: [
                    {
                        label: "Help",
                        accelerator: process.platform === 'darwin' ? 'F1' : 'F1',
                        click: () => {
                            console.log("Help...")
                            this.eventMan?.handleEvent("help")
                        }
                    }, {
                        label: "Contact Support",
                        click: () => {
                            this.eventMan?.handleEvent("support")
                        }
                    }, {
                        label: "Bugs Report",
                        click: () => {
                            this.eventMan?.handleEvent("reports")
                        }
                    }, {
                        label: "Feed Back",
                        click: () => {
                            this.eventMan?.handleEvent("feedback")
                        }
                    },
                ]
            },
            "About": {
                submenu: [
                    {
                        label: "Check for Updates",
                        click: () => {
                            this.eventMan?.handleEvent("updates")
                        }
                    }
                ]
            },
        })
    }

    setEventManager(eventMan: EventManager): MenuFactory {
        this.eventMan = eventMan
        return this
    }

    setItem(item: { [key: string]: any }): MenuFactory {
        Object.assign(this.items, item)
        return this
    }

    build(): Menu {
        Object.keys(this.items).forEach((item) => {
            const sub: any = this.items[item]
            this.menu.append(new MenuItem({
                label: item,
                ...sub
            }))
        })
        return this.menu
    }
}

export default MenuFactory