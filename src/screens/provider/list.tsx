import React from "react";
import {
  CartIcon,
  CloseIcon,
  ProviderIcon,
  SearchIcon,
} from "../../core/graphx/icons";
import cclass from "../../core/graphx/helpers/cclass";
import style from "./style.scss";
import Card from "../../core/graphx/widgets/cards";
import InputField from "../../core/graphx/forms/field";
import Button from "../../core/graphx/forms/button";

const ListProvider = (props: any): any => {
  return (
    <div id={style.provider}>
      <div className={cclass([style.container])}>
        <form
          action="#"
          method="get"
          className={cclass([style.form, "form", "search-form"])}
        >
          <div className="field">
            <InputField
              type="text"
              name="s"
              placeholder="Search"
              IconRight={<SearchIcon size={15} />}
            />
          </div>
        </form>
        <div className={cclass([style.content])}>
          <div style={{ padding: 40, overflow: "auto" }}>
            <div className={cclass([style.items])}>
              <div className={style.item}>
                <Card Icon={<ProviderIcon size={16} />}>
                  <div style={{ textAlign: "left" }}>
                    <div className="h3">My Provider</div>
                    <div className="">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit,
                      amet consectetur adipisicing elit.
                    </div>
                  </div>
                </Card>
              </div>
              <div className={style.item}>
                <Card Icon={<ProviderIcon size={16} />}>
                  <div style={{ textAlign: "left" }}>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit
                    amet consectetur adipisicing elit.
                  </div>
                </Card>
              </div>
              <div className={style.item}>
                <Card Icon={<ProviderIcon size={16} />}>
                  <div style={{ textAlign: "left" }}>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit
                    amet consectetur adipisicing elit.
                  </div>
                </Card>
              </div>
              <div className={style.item}>
                <Card Icon={<ProviderIcon size={16} />}>
                  <div style={{ textAlign: "left" }}>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit
                    amet consectetur adipisicing elit.
                  </div>
                </Card>
              </div>
              <div className={style.item}>
                <Card Icon={<ProviderIcon size={16} />}>
                  <div style={{ textAlign: "left" }}>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit
                    amet consectetur adipisicing elit.
                  </div>
                </Card>
              </div>
              <div className={style.item}>
                <Card Icon={<ProviderIcon size={16} />}>
                  <div style={{ textAlign: "left" }}>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit
                    amet consectetur adipisicing elit.
                  </div>
                </Card>
              </div>
              <div className={style.item}>
                <Card Icon={<ProviderIcon size={16} />}>
                  <div style={{ textAlign: "left" }}>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit
                    amet consectetur adipisicing elit.
                  </div>
                </Card>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={cclass([style.sideright])}>
        <form
          action="#"
          method="post"
          className={cclass([style.form, "form", "pad-sm"])}
        >
          <div className={cclass([style.fieldset])}>
            <div className={cclass([style.fields])}>
              <div className={cclass([style.field])}>
                <InputField
                  type="text"
                  name="name"
                  placeholder="Name"
                  label="Provider name"
                />
              </div>
            </div>
            <div className={cclass([style.fields])}>
              <div className={cclass([style.field])}>
                <InputField
                  type="text"
                  name="ref"
                  placeholder="Reference"
                  label="Reference"
                />
              </div>
            </div>
            <div className={cclass([style.fields, style.col2])}>
              <div className={cclass([style.field])}>
                <InputField
                  type="text"
                  name="phone"
                  placeholder="Phone"
                  label="Phone"
                />
              </div>
              <div className={cclass([style.field])}>
                <InputField
                  type="text"
                  name="email"
                  placeholder="Email"
                  label="Email"
                />
              </div>
            </div>
            <div className={cclass([style.fields])}>
              <div className={cclass([style.field])}>
                <InputField
                  type="text"
                  name="address"
                  placeholder="Address"
                  label="Address"
                />
              </div>
            </div>
          </div>
          <div className={cclass([style.actions])}>
            <div className={cclass([style.action])}>
              <Button label="Apply" />
            </div>
            <div className={cclass([style.action])}>
              <button className="btn disabled" type="button">
                Cancel
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ListProvider;
