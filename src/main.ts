import {app, BrowserWindow, globalShortcut, Menu, Tray} from 'electron';
import * as child from "child_process"
import * as path from "path";
import * as fs from "fs";
import * as os from "os";
import TaskManager from "./core/manager/task";
import MenuFactory from "./core/factory/menu";
import EventManager, {Event} from "./core/manager/event";
import logger from "./core/logger";

let home: BrowserWindow = null
let splash: BrowserWindow = null
let settings: BrowserWindow = null
let appIcon: Tray = null
const menu = new MenuFactory()
const eventManager = new EventManager()


// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
    app.quit();
}
const createSplash = async (taskMan: TaskManager) => {
    const splashTask = await taskMan.getTask("splash")
    splash = splashTask.window
    splash?.show()
}
const createHome = async (taskMan: TaskManager) => {
    const homeTask = await taskMan.getTask("home")
    home = homeTask.window
    home?.show()
    home?.webContents.openDevTools()
}
const createSettings = async (taskMan: TaskManager) => {
    const settingTask = await taskMan.getTask("settings")
    settings = settingTask.window
    settings?.setParentWindow(home)
    settings?.show()
}
const schema_db = path.join(os.homedir(), ".im", "imcom", "schema.json")

const checkImstore = (): Promise<string> => {
    let version: any = null
    try {
        version = child.execSync("imstore --version", {"encoding": "utf-8"})
    } catch (e: any) {
        logger.debug(e.message)
        // throw  new Error(e)
    }
    logger.info(`imstore version: ${version}`)
    return version
}

const initDb = () => {
    let out = null
    try {
        // TODO we should get the current time from an internet api ...
        // TODO we have to get the configs from the web and put in his directory
        // time is important
        out = child.execSync(`imstore init -i ${schema_db} -a imcom`, {"encoding": "utf-8"});
        console.log(out)
    } catch (e: any) {
        logger.debug(e.message)
        // throw new Error(e)
    }
    return out
}
const createWindow = async (): Promise<any> => {
    try {
        const taskMan = new TaskManager()
        await createSplash(taskMan)

        eventManager.subscribe(new Event("preferences", (e) => {
            logger.info(e.message)
        }, async () => {
            await createSettings(taskMan)
        }))

        logger.info("checking storage...")
        const hasImstore = checkImstore()
        const dbReady = initDb()
        if (hasImstore && dbReady) {
            menu.setEventManager(eventManager)
            Menu.setApplicationMenu(menu.build())
            const iconPath = path.join(__dirname, "assets/img/favicon-16x16.png")
            appIcon = new Tray(iconPath)
            appIcon.setContextMenu(Menu.buildFromTemplate([
                {
                    label: 'Open', click: async () => {
                        if (home?.isDestroyed()) {
                            await createHome(taskMan)
                        } else {
                            home?.focus()
                        }
                    }
                }, {
                    label: 'Register', click: () => {
                        logger.info("Handling registration...")
                    }
                }, {
                    label: 'Check for Updates', click: () => {
                        logger.info("Checking for update...")
                        app.quit()
                        app.relaunch()
                    }
                }, {
                    label: 'Quit', click: () => {
                        app.quit()
                    }
                }
            ]))
            splash?.close()
            await createHome(taskMan)
        } else {
            logger.debug(`Database not found quitting the application...`)
            app.quit()
        }

    } catch (e) {
        logger.error(`Error while creating Window: ${e.message}`)
    }
};


app.whenReady().then(() => {
    globalShortcut.register('Alt+CommandOrControl+F', () => {
        logger.info('Electron loves global shortcuts!')
    })
})


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => await createWindow());

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', async () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        await createWindow();
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
