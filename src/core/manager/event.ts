export class Event {
    name: string
    error: (args?: any) => void
    callback: (args?: any) => void

    constructor(name: string, error: (args?: any) => void, callback: (args?: any) => void) {
        this.name = name
        this.error = error
        this.callback = callback
    }
}

class EventManager {
    private readonly subscribers: Array<Event> = []

    subscribe(event: Event): EventManager {
        this.subscribers.push(event)
        return this
    }

    handleEvent(name: string): void {
        this.subscribers.forEach(async (ev) => {
            try {
                if (ev.name == name) {
                    await ev.callback()
                }
            } catch (e) {
                ev.error(e)
            }
        })
    }
}


export default EventManager