// This allows TypeScript to pick up the magic constant that's auto-generated by Forge's Webpack
// plugin that tells the Electron app where to look for the Webpack-bundled app code (depending on
// whether you're running in development or production).
import {BrowserWindow} from "electron";
import mainConfig from "../../screens/main/config.json"
import splashConfig from "../../screens/splash/config.json"
import settingsConfig from "../../screens/settings/config.json"
import homeConfig from "../../screens/home/config.json"

declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
declare const SPLASH_WINDOW_WEBPACK_ENTRY: string;
declare const HOME_WINDOW_WEBPACK_ENTRY: string;
declare const SETTINGS_WINDOW_WEBPACK_ENTRY: string;

interface TaskInterface {
    entry: string
    preload: string
    config: any
    window?: BrowserWindow
}

class Task implements TaskInterface {
    entry: string;
    preload: string;
    config: any;
    window?: BrowserWindow;

    constructor(arg: TaskInterface) {
        this.entry = arg.entry
        this.preload = arg.preload
        this.config = arg.config
        this.window = arg.window
    }

    setConfig(config: any) {
        if(typeof config == "object") {
            this.config = Object.assign(this.config, config)
        }
        return this
    }
}

class TaskManager {
    private readonly tasks: { [key: string]: Task } = {
        "main": new Task({entry: MAIN_WINDOW_WEBPACK_ENTRY, preload: "", config: mainConfig}),
        "splash": new Task({entry: SPLASH_WINDOW_WEBPACK_ENTRY, preload: "", config: splashConfig}),
        "home": new Task({entry: HOME_WINDOW_WEBPACK_ENTRY, preload: "", config: homeConfig}),
        "settings": new Task({entry: SETTINGS_WINDOW_WEBPACK_ENTRY, preload: "", config: settingsConfig}),
    }

    async getTask(name: string): Promise<Task> {
        try {
            const task = this.tasks[name]
            const win = new BrowserWindow(task.config)
            await win.loadURL(task.entry)
            task.window = win
            return task
        } catch (e) {
            console.error("getTask", e.message)
        }
    }

}

export default TaskManager