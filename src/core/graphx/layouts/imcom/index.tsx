import React from "react"
import cclass from "../../helpers/cclass"
import style from "./style.scss"


const ImCom = (props:any)=>{
    const { children, className } = props
    return ( 
        <div id="imcom" className={cclass([className, style.layout])}>
            {children}
        </div>
    )
}

export default ImCom
